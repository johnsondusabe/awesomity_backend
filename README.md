## Introdcution
EMS is an employee managment system which help an organisation to keet track of their employees and their department

## User
- Manager
- employee

## DB setup
![ERD](assets/erd.png)
## Functionality

### MANAGER
- [X]  Manager signup
- [X]  Manager signin
- [X]  Manager search employee
- [X]  Manager reset password
- [X]  Manager verify account
- [X]  Add employee
- [X]  update employee
- [X]  delete employee
- [X]  activate employee
- [X]  suspend employee
- [X]  assign Department

## Installation Guide
To install this project you have to clone into your local setup
### Requirements
- Node (obviously)
- DB(relational Database)
- NPM

### Pre-setup
Create a `.env` file then copy and paste data from `.env-example` into Your newly created `.env`,
make sure you fill all the information according to your setup
### Commands

#### install dependencies
```shell
npm i
```

#### Run Migration
```shell
npm run migrate
```

#### Run Application
```shell
npm start
```
#### Run Test
```shell
npm run test 
```




## Documentation
[docs](https://documenter.getpostman.com/view/12149008/UV5WDdin)
